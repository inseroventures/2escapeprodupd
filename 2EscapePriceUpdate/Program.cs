﻿using System;

using System.Linq;
using ShopifySharp;
using ShopifySharp.Filters;
using System.IO;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace _2EscapePriceUpdate
{
    class Program
    {
        private static ProductService service;
        private static ExcelWorksheet pimsheet, psheet;
        private static ProductService pservice;
        private static Product product = null;

        private static int FindWorksheet(ExcelPackage infile, string worksheetName)
        {
            var worksheetCounts = infile.Workbook.Worksheets.Count;
            var found = false;
            int i = 1;

            while (!found && i <= worksheetCounts)
            {
                if (infile.Workbook.Worksheets[i].Name.ToUpper() == worksheetName.ToUpper())
                {
                    found = true;
                }
                else
                {
                    i += 1;
                }
            }

            if (!found)
            {
                Console.WriteLine($"Cannot find worksheet {worksheetName}");
                i = 0;
            }
            return i;
        }

        static async void UpdateShopProd()
        {
            
            var aproduct = await pservice.UpdateAsync((long)product.Id, new Product()
            {
                Tags = product.Tags,
                Variants = product.Variants
            });
        }

        static void UpdateShopify()
        {
            pservice = new ProductService("https://2escapemy.myshopify.com", "699148d56577459395781a2b7d5207f0");
            Console.WriteLine("Opening New Price Spreadsheet");
            var startRow = 15; // Adjust these two to get all the rows. 
            var endRow = 303;
            var inpath = "C:\\Mystuff\\Pricing Update\\Felt Kona pricing 26 Jul 2017.xlsx";
            var pfile = new ExcelPackage(new FileInfo(inpath));

            var i = FindWorksheet(pfile, "EXPORT FIXED VAL");


   
            psheet = pfile.Workbook.Worksheets[i];
            string pTitle = "";
            string rTitle = "";
            for (var r = startRow; r <= endRow; r++)
                {
                    //check if already processed product - assumption is that every variant has the same price
                    if(rTitle == psheet.Cells[r, 4].Text)
                    {
                        continue;
                    }
                    else
                    {
                        rTitle = psheet.Cells[r, 4].Text;
                    }
                    pTitle = psheet.Cells[r, 5].Text + " " + psheet.Cells[r, 4].Text;
                    if (!String.IsNullOrEmpty(psheet.Cells[r, 7].Text)) pTitle = pTitle + " " + psheet.Cells[r, 7].Text;
                    Console.WriteLine(pTitle + " " + psheet.Cells[r, 116].Text);

                    // find product
                    var filter = new ProductFilter()
                    {
                        Handle = (string)psheet.Cells[r, 116].Value
                    };

                    System.Threading.Thread.Sleep(1000);
                    var x = pservice.ListAsync(filter).Result.ToList();
                    product = x[0];

                    if(product.Title==pTitle)
                    {
                        //update Esc$_nnn in product.tags
                        var tags = product.Tags;
                        var pt = Regex.Match(product.Tags, @".Esc\$_(\d+),.");
                        product.Tags = tags.Replace("Esc$_" + pt.Groups[1].Value, "Esc$_" + (Int32.Parse(psheet.Cells[r, 38].Value.ToString()) * 100).ToString());
                        Console.WriteLine("... Tags:" + product.Tags);

                        //update MSRP price in Variant
                        foreach (var prodVariant in product.Variants)
                        {

                            prodVariant.Price = Math.Round(Convert.ToDecimal(psheet.Cells[r, 35].Text), 2);
                            prodVariant.CompareAtPrice = Math.Round(Convert.ToDecimal(psheet.Cells[r, 27].Text), 2); 

                        }

                        UpdateShopProd();

                    }
                    else
                    {
                        Console.WriteLine("Title mismatch - havent found correct product" + product.Title);

                        Console.ReadLine();
                        Environment.Exit(0);
                    }

                }

            Console.WriteLine("Completed");
            Console.ReadLine();
        }
        static void UpDatePIM()
        {

            Console.WriteLine("Opening PIM");
            var pimstartRow = 4; // Adjust these two to get all the rows. 
            var pimendRow = 3053;
            var pimpath = "C:\\Mystuff\\PIM\\PIM 19-07-17 AH.xlsx";
            var pimfile = new ExcelPackage(new FileInfo(pimpath));
            var pimi = FindWorksheet(pimfile, "DATA ENTRY");
            pimsheet = pimfile.Workbook.Worksheets[pimi];

            Console.WriteLine("Opening New Price Spreadsheet");
            var startRow = 3; // Adjust these two to get all the rows. 
            var endRow = 303;
            var ppath = "C:\\Mystuff\\Pricing Update\\Felt Kona pricing 26 Jul 2017.xlsx";
            var pfile = new ExcelPackage(new FileInfo(ppath));

            var i = FindWorksheet(pfile, "EXPORT FIXED VAL");
            psheet = pfile.Workbook.Worksheets[i];


            for (var r = startRow; r <= endRow; r++)
            {
                Console.WriteLine(psheet.Cells[r, 1].Text + " " + psheet.Cells[r, 4].Text + " " + psheet.Cells[r, 27].Text);

                for (var pimr = pimstartRow; pimr <= pimendRow; pimr++)
                {
                    if (psheet.Cells[r, 1].Text == pimsheet.Cells[pimr, 2].Text)
                    {
                        Console.WriteLine(" ...." + pimsheet.Cells[pimr, 2].Text + " C:" + pimsheet.Cells[pimr, 154].Text + " M:" + pimsheet.Cells[pimr, 155].Text + " R:" + pimsheet.Cells[pimr, 156].Text);

                        pimsheet.Cells[pimr, 154].Value = psheet.Cells[r, 33].Value;
                        pimsheet.Cells[pimr, 155].Value = psheet.Cells[r, 27].Value;
                        pimsheet.Cells[pimr, 156].Value = psheet.Cells[r, 35].Value;
                        pimsheet.Cells[pimr, 152].Value = Int32.Parse(psheet.Cells[r, 38].Value.ToString())*100;

                        break;
                    }

                }


            }

            pimfile.Save();
            Console.WriteLine("Completed");
            Console.ReadLine();
        }


    

        static void Main(string[] args)
        {
           // UpDatePIM();
           UpdateShopify();
            //UpDatePIMWeb();

        }
    }
}
